package server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

public class MyHttpResponder {

    private static final String STARTLINE = "HTTP/1.1 200 OK\n";
    private static final String NOAUTHORIZATION = "HTTP/1.1 401 Unauthorized\n";
    private static final String AUTHENTICATIONHEADER = "WWW-Authenticate: Basic realm=\"nmrs_m7VKmomQ2YM3:\"\n";
    private static final String TYPEHEADER = "content-type: text/plain; charset=utf-8\n";
    private static final String BADREQUEST = "HTTP/1.1 400 Bad Request: ";

    public static void respond(MyHttpRequest req, DataOutputStream dos) throws IOException {
        
        if(req.getHeader("authorization") != null){
            dos.writeBytes(STARTLINE);
        } else {
                dos.writeBytes(NOAUTHORIZATION);
                dos.writeBytes(AUTHENTICATIONHEADER);
        }
        
        dos.writeBytes(TYPEHEADER);
        dos.writeBytes("\n");


        dos.writeBytes("Hello " + req.getHeader("user-agent") +"\n");
        dos.writeBytes("Today is " + new Date().toString() + "\n");
        dos.close();
    }

    public static void reportError(String message, DataOutputStream dos) throws IOException {
        dos.writeBytes(BADREQUEST + message);
        dos.writeBytes("\n\n");
        dos.close();

    }
}
